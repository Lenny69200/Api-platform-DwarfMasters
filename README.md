# Api Platform DwarfMasters


## Pour lancer l'application

Cloner le projet pour le récupérer sur votre environement local

```
Git Clone 
```

Télécharger l'image puis lancez vos containers 

```
docker compose build
docker compose up -d
```


Ensuite rentrez dans votre container applicatif

```
docker compose exec app bash 
```


Executez la commande pour mettre à jour tout les dépendances 

``` 
composer install
```

Pour la base de donnée metter à jour le schéma de la base de donnée
    
```
docker compose exec app php bin/console doctrine:schema:update --force
```

Pour les migrations 

```
docker compose exec app php bin/console doctrine:migrations:migrate
```

Générer les Fixtures

```
docker compose exec app php bin/console doctrine:fixtures:load
```

Pour lancer les clés priver / public pour l'authentification

```
docker compose exec app php bin/console lexik:jwt:generate-keypair
```
## Pour restart la bdd 

Détruire la bdd existant :

```
php bin/console doctrine:database:drop --force
```

Créer une nouvelle bdd :

```
php bin/console doctrine:database:create
```

Mettre à jour le schéma de la bdd :

```
php bin/console doctrine:schema:update --force
```

Lancer les migrations :

```
php bin/console doctrine:migrations:migrate
```

## Pour mettre à jour son projet lors d'une mr / changement sur le repo distant

Pull les changements 

```
git pull
```

composer 

```
composer install
```

### Bravo Vous savez Lire une Documentation 

> Accèder aux projets : http://localhost:8000/api   