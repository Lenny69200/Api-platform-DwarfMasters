<?php

namespace App\EventSubscriber;

use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\ValueObject\EmailEnum;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use App\Entity\Contact;
use Symfony\Component\HttpFoundation\Request;

class ContactMailSubscriber implements EventSubscriberInterface
{
    public function __construct(private MailerInterface $mailer)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['sendMail', EventPriorities::POST_WRITE],
        ];
    }

    public function sendMail($event): void
    {
        $contact = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$contact instanceof Contact || Request::METHOD_POST !== $method) {
            return;
        }

        $htmlContent = "
    <html>
        <head>
            <style>
                .email-card {
                    font-family: Arial, sans-serif;
                    width: 600px;
                    margin: 0 auto;
                    border: 1px solid #ddd;
                    border-radius: 5px;
                    padding: 20px;
                    box-shadow: 0 0 10px rgba(0,0,0,0.1);
                }
                .email-card h1 {
                    color: #333;
                }
                .email-card p {
                    color: #666;
                }
                .email-card .label {
                    font-weight: bold;
                }
                .email-card .logo {
                    display: block;
                    margin: 0 auto;
                }
            </style>
        </head>
        <body>
            <div class='email-card'>
                <h1>Comité de fetes vaulx millieu</h1>
                <hr>
                <p><span class='label'>Nom:</span> " . htmlspecialchars($contact->getName()) . "</p>
                <p><span class='label'>Prénom:</span> " . htmlspecialchars($contact->getLastname()) . "</p>
                <p><span class='label'>Email:</span> " . htmlspecialchars($contact->getEmail()) . "</p>
                <p><span class='label'>Message:</span> " . htmlspecialchars($contact->getMessage()) . "</p>
                <hr>
                <p><span class='label'>Contact de l'association :</span> ". EmailEnum::EmailAddress->value() . " </p>
            </div>
        </body>
    </html>
    ";

        $message = (new Email())
            ->from($contact->getEmail())
            ->to(EmailEnum::EmailAddress->value)
            ->subject('Message de contact')
            ->html($htmlContent);

        $this->mailer->send($message);
    }
}

