<?php

namespace App\EventSubscriber;

use ApiPlatform\Symfony\EventListener\EventPriorities;
use App\Entity\Reservation;
use App\ValueObject\EmailEnum;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;

class ReservationMailSubscriber implements EventSubscriberInterface
{
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['sendMail', EventPriorities::POST_WRITE],
        ];
    }

    public function sendMail(ViewEvent $event): void
    {
        $reservation = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$reservation instanceof Reservation || Request::METHOD_POST !== $method) {
            return;
        }

        $associationEmail = $reservation->getAssociation()?->getEmail();
        $particulierEmail = $reservation->getParticulier()?->getEmail();

        if ($associationEmail) {
            $contactEmail = $associationEmail;
            $contactLabel = 'Association';
        } elseif ($particulierEmail) {
            $contactEmail = $particulierEmail;
            $contactLabel = 'Particulier';
        }

        $htmlContent = "
<html>
    <head>
        <style>
            .email-card {
                font-family: Arial, sans-serif;
                width: 600px;
                margin: 0 auto;
                border: 1px solid #ddd;
                border-radius: 5px;
                padding: 20px;
                box-shadow: 0 0 10px rgba(0,0,0,0.1);
                text-align: center;
            }
            .email-card h1 {
                text-align: center;
                color: #333;
            }
            .email-card p {
                color: #666;
                text-align: left;
            }
            .email-card .label {
                font-weight: bold;
            }
            .email-card .button {
                display: inline-block;
                padding: 10px 20px;
                font-size: 16px;
                font-weight: bold;
                color: #fff;
                background-color: #007bff;
                text-decoration: none;
                border-radius: 5px;
                margin-top: 20px;
                text-align: center;
            }
            .email-card .button:hover {
                background-color: #0056b3;
            }
        </style>
    </head>
    <body>
        <div class='email-card'>
            <h1>Nouvelle réservation de matériel</h1>
            <hr>
            <p><span class='label'>Date de réservation:</span> " . htmlspecialchars($reservation->getDateReservation()->format('d-m-Y H:i')) . "</p>
            <p><span class='label'>Date de retour:</span> " . htmlspecialchars($reservation->getDateRetour()->format('d-m-Y H:i')) . "</p>
            <p><span class='label'>$contactLabel:</span> " . htmlspecialchars($contactEmail) . "</p>
            <p><span class='label'>Statut:</span> " . htmlspecialchars($reservation->getStatutReservation()?->getLibelle() ?? 'N/A') . "</p>
            <hr>
            <a href='http://localhost:5173/ReservationManagement/' class='button'>Voir le matériel demandé</a>
        </div>
    </body>
</html>
";

        $message = (new Email())
            ->from('no-reply@reservation.com')
            ->to(EmailEnum::EmailAddress->value())
            ->subject('Nouvelle réservation de matériel')
            ->html($htmlContent);

        $this->mailer->send($message);
    }
}
