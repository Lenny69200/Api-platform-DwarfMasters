<?php

namespace App\Controller\Materiel\Post;

use App\Entity\Materiel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MaterielController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/api/materiels", name="api_materiels_create", methods={"POST"})
     */
    public function __invoke(Request $request): Response
    {
        // Récupère les données du formulaire
        $libelle = $request->request->get('libelle');
        $prix = $request->request->get('prix');
        $pourAssociation = filter_var($request->request->get('pourAssociation'), FILTER_VALIDATE_BOOLEAN);
        $nbExemplaires = $request->request->get('nbExemplaires');
        $nbExemplairesDispo = $request->request->get('nbExemplairesDispo');

        // Crée une nouvelle instance de Materiel 
        $materiel = new Materiel();
        $materiel->setLibelle($libelle);
        $materiel->setPrix($prix);
        $materiel->setPourAssociation($pourAssociation);
        $materiel->setNbExemplaires($nbExemplaires);
        $materiel->setNbExemplairesDispo($nbExemplairesDispo);

        // Gestion de l'image
        $imageFile = $request->files->get('imageFile');
        if ($imageFile) {
            $fileName = md5(uniqid()) . '.' . $imageFile->guessExtension();
            $imageFile->move(
                $this->getParameter('kernel.project_dir') . '/public/uploads/materiels',
                $fileName
            );
            $materiel->setImage($fileName);
        }

        $this->entityManager->persist($materiel);
        $this->entityManager->flush();

        return $this->json(['message' => 'Matériel créé!']);
    }
}
