<?php

namespace App\Controller\Materiel\Update;

use App\Entity\Materiel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MaterielUpdateController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/api/materiels/{id}", name="api_materiels_update", methods={"POST"})
     */
    public function __invoke(Request $request, $id): Response
    {
        $materiel = $this->entityManager->getRepository(Materiel::class)->find($id);

        if (!$materiel) {
            return $this->json(['message' => 'Matériel non trouvé!'], Response::HTTP_NOT_FOUND);
        }
        
        $libelle = $request->request->get('libelle');
        $prix = $request->request->get('prix');
        $pourAssociation = filter_var($request->request->get('pourAssociation'), FILTER_VALIDATE_BOOLEAN);
        $nbExemplaires = $request->request->get('nbExemplaires');
        $nbExemplairesDispo = $request->request->get('nbExemplairesDispo');

        $materiel->setLibelle($libelle);
        $materiel->setPrix($prix);
        $materiel->setPourAssociation($pourAssociation);
        $materiel->setNbExemplaires($nbExemplaires);
        $materiel->setNbExemplairesDispo($nbExemplairesDispo);

        $imageFile = $request->files->get('imageFile');

        if ($imageFile) {
            // Suppression de l'ancienne image
            $oldImage = $materiel->getImage();
            if ($oldImage) {
                $oldImagePath = $this->getParameter('kernel.project_dir') . '/public/uploads/materiels/' . $oldImage;
                if (file_exists($oldImagePath)) {
                    unlink($oldImagePath);
                }
            }

            // Upload de la nouvelle image
            $fileName = md5(uniqid()) . '.' . $imageFile->guessExtension();
            $imageFile->move(
                $this->getParameter('kernel.project_dir') . '/public/uploads/materiels',
                $fileName
            );
            $materiel->setImage($fileName);
        }

        $this->entityManager->flush();

        return $this->json(['message' => 'Matériel mis à jour!']);
    }
}
