<?php

namespace App\Controller\Media\Update;

use App\Entity\Media;
use App\Entity\Evenement;
use App\Entity\TypeMedia;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MediaUpdateController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/api/medias/{id}", name="api_media_update", methods={"POST"})
     */
    public function __invoke(Request $request, $id): Response
    {
        $media = $this->entityManager->getRepository(Media::class)->find($id);

        if (!$media) {
            return $this->json(['message' => 'Média non trouvé!'], Response::HTTP_NOT_FOUND);
        }

        $description = $request->request->get('description');
        $evenementId = $request->request->get('evenement');
        $typeMedia = $request->request->get('typeMedia');


        $evenement = $this->entityManager->getRepository(Evenement::class)->find($evenementId);



        if (!$evenement) {
            return $this->json(['error' => 'Média spécifié n\'existe pas.'], Response::HTTP_NOT_FOUND);
        }


        $type = $this->entityManager->getRepository(TypeMedia::class)->find($typeMedia);



        $media->setDescription($description);
        $media->setEvenement($evenement);
        $media->setTypeMedia($type);

        $imageFile = $request->files->get('imageFile');

        if ($imageFile) {

            // Upload de la nouvelle image
            $fileName = md5(uniqid()) . '.' . $imageFile->guessExtension();
            $imageFile->move(
                $this->getParameter('kernel.project_dir') . '/public/uploads/medias',
                $fileName
            );
            $media->setChemin($fileName);
        }

        $this->entityManager->flush();

        return $this->json(['message' => 'Média mis à jour!']);
    }
}
