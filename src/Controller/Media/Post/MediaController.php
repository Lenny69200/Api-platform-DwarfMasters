<?php

namespace App\Controller\Media\Post;

use App\Entity\Media;
use App\Entity\Evenement;
use App\Entity\TypeMedia;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class MediaController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/api/medias", name="api_media_create", methods={"POST"})
     */
    public function __invoke(Request $request): Response
    {
      
        $description = $request->request->get('description');
        $evenementId = $request->request->get('evenement'); 
        $typeMedia = $request->request->get('typeMedia');

       
        $evenement = $this->entityManager->getRepository(Evenement::class)->find($evenementId);



        if (!$evenement) {
            return $this->json(['error' => 'L\'événement spécifié n\'existe pas.'], Response::HTTP_NOT_FOUND);
        }
        

        $type = $this->entityManager->getRepository(TypeMedia::class)->find($typeMedia);

 
        $media = new Media();
        $media->setDescription($description);
        $media->setEvenement($evenement); 
        $media->setTypeMedia($type);

   
        // Gestion de l'image
        $imageFile = $request->files->get('imageFiles');

        if ($imageFile) {
            $fileName = md5(uniqid()) . '.' . $imageFile->guessExtension();
            $imageFile->move(
                $this->getParameter('kernel.project_dir') . '/public/uploads/medias',
                $fileName
            );
            $media->setChemin($fileName);
        } else {
            return $this->json(['error' => 'Aucun fichier image trouvé.'], Response::HTTP_BAD_REQUEST);
        }
        
        


        $this->entityManager->persist($media);
        $this->entityManager->flush();

        return $this->json(['message' => 'Media créé!']);
    }
}
