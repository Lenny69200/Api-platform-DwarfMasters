<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends AbstractController
{
    #[Route('/api/login', name: 'app_login', methods: ['POST'])]
    public function login(): JsonResponse
    {
        $user = $this->getUser();


        // Vérifier si un utilisateur est authentifié
        if (!$user) {
            return $this->json(['message' => 'Aucun utilisateur n\'est authentifié.'], 401);
        }

        // Renvoyer les informations de l'utilisateur au format JSON
        return $this->json([
            'username' => $user->getUserIdentifier(),
            'roles' => $user->getRoles(),

        ]);
    }

    #[Route('/api/logout', name: 'app_logout', methods: ['POST'])]
    public function logout(): JsonResponse
    {
        return $this->json(['message' => 'Déconnexion réussie.']);
    }

    #[Route('/api/moi', name: 'app_me', methods: ['GET'])]
    public function me(): JsonResponse
    {
        $user = $this->getUser();

        // Vérifier si un utilisateur est authentifié
        if (!$user) {
            return $this->json(['message' => 'Aucun utilisateur n\'est authentifiéaaa.'], 401);
        }

        // Renvoyer les informations de l'utilisateur au format JSON
        return $this->json([
            'username' => $user->getUserIdentifier(),
            'roles' => $user->getRoles(),
        ]);
    }
}
