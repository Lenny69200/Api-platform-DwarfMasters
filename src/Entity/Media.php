<?php

namespace App\Entity;


use ApiPlatform\Metadata\ApiResource;
use App\Repository\MediaRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use App\Controller\Media\Post\MediaController;
use App\Controller\Media\Update\MediaUpdateController;

#[ORM\Entity(repositoryClass: MediaRepository::class)]
#[ApiResource(
    operations: [
        new Get(),
        new GetCollection(),
        new Post(
            controller: MediaController::class,
            deserialize: false,
        ),
        new Patch(),
        new Put(),
        new Delete(),
    ],
    // normalizationContext: ['groups' => ['media:read']],
    // denormalizationContext: ['groups' => ['media:write']]
)]

#[Vich\Uploadable]
class Media
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups('evenement:read')]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups('evenement:read')]
    private ?string $chemin = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups('evenement:read')]
    private ?string $description = null;

    #[ORM\ManyToOne(targetEntity: Evenement::class, inversedBy: "media")]
    #[ORM\JoinColumn(nullable: true)]
    private ?Evenement $evenement = null;

    #[ORM\ManyToOne(targetEntity: TypeMedia::class, inversedBy: "media")]
    #[ORM\JoinColumn(nullable: true)]
    private ?TypeMedia $typeMedia = null;


    /**
     * @Vich\UploadableField(mapping="medias", fileNameProperty="chemin")
     */
    #[Vich\UploadableField(mapping: 'medias', fileNameProperty: 'chemin')]
    private ?File $imageFile = null;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChemin(): ?string
    {
        return $this->chemin;
    }

    public function setChemin(?string $chemin): static
{
    $this->chemin = $chemin ? : '';

    return $this;
}


    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getEvenement(): ?Evenement
    {
        return $this->evenement;
    }

    public function setEvenement(?Evenement $evenement): void
    {
        $this->evenement = $evenement;
    }

    public function getTypeMedia(): ?TypeMedia
    {
        return $this->typeMedia;
    }

    public function setTypeMedia(?TypeMedia $typeMedia): void
    {
        $this->typeMedia = $typeMedia;
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageFile(?File $imageFile): void
    {
        $this->imageFile = $imageFile;
    }

    #[Groups(['medias:read'])]
    public function getImageUrl(): ?string
    {
        return $this->chemin ? 'uploads/medias/' . $this->chemin : null;
    }
}
