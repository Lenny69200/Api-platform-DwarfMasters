<?php
namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\UtilisateurRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UtilisateurRepository::class)]
#[ApiResource]
class Utilisateur implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $login = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $password = null;

    #[ORM\ManyToOne(targetEntity: Role::class, inversedBy: "utilisateurs")]
    private Role $role;

    #[ORM\OneToOne(targetEntity: Association::class, mappedBy: 'utilisateur', cascade: ['persist', 'remove'])]
    private ?Association $association = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): static
    {
        $this->login = $login;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): static
    {
        $this->password = $password;

        return $this;
    }

    public function getRole(): Role
    {
        return $this->role;
    }

    public function setRole(Role $role): void
    {
        $this->role = $role;
    }

    public function getAssociation(): ?Association
    {
        return $this->association;
    }

    public function setAssociation(?Association $association): static
    {
        if ($association === null && $this->association !== null) {
            $this->association->setUtilisateur(null);
        }

        if ($association !== null && $association->getUtilisateur() !== $this) {
            $association->setUtilisateur($this);
        }

        $this->association = $association;

        return $this;
    }

    public function getRoles(): array
    {
        return [$this->role->getLibelle()];
    }

    public function getSalt(): ?string
    {
        return null;
    }

    public function getUsername(): string
    {
        return $this->login;
    }

    public function eraseCredentials(): void
    {
        // Null implementation
    }

    public function getUserIdentifier(): string
    {
        return $this->login;
    }
}
