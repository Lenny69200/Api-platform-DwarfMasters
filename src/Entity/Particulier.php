<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiResource;
use App\Filter\ParticulierEmailFilter;
use App\Repository\ParticulierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ParticulierRepository::class)]
#[ApiResource]
#[ApiFilter(SearchFilter::class, properties: ['email' => 'exact'])]
#[ApiFilter(ParticulierEmailFilter::class)]
class Particulier
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups('reservation:read')]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups('reservation:read')]
    private ?string $nom = null;

    #[ORM\Column(length: 255)]
    #[Groups('reservation:read')]
    private ?string $prenom = null;

    #[ORM\Column(length: 255)]
    #[Groups('reservation:read')]
    private ?string $telephone = null;

    #[ORM\OneToMany(targetEntity: Reservation::class, mappedBy: 'particulier', cascade: ['remove'])]
    private Collection $reservation;

    public function __construct()
    {
        $this->reservation = new ArrayCollection();
    }
    #[ORM\Column(length: 255)]
    #[Groups('reservation:read')]
    private ?string $email = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): static
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): static
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection<int, Reservation>
     */
    public function getReservation(): Collection
    {
        return $this->reservation;
    }

    public function addReservation(Reservation $reservation): static
    {
        if (!$this->reservation->contains($reservation)) {
            $this->reservation->add($reservation);
            $reservation->setParticulier($this);
        }

        return $this;
    }

    public function removeReservation(Reservation $reservation): static
    {
        if ($this->reservation->contains($reservation)) {
            $this->reservation->removeElement($reservation);
            if ($reservation->getParticulier() === $this) {
                $reservation->setParticulier(null);
            }
        }
        return $this;
    }
}
