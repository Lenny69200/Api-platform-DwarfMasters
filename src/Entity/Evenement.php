<?php

namespace App\Entity;


use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use App\Filter\DateEvenementFilter;
use App\Filter\DateEvenementYearFilter;
use App\Filter\DateNextEvenementFilter;
use App\Filter\DatePreviousEvenementFilter;
use App\Filter\EvenementIsActualite;
use App\Repository\EvenementRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: EvenementRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['evenement:read']],
)]
#[ApiFilter(DateNextEvenementFilter::class, properties: ['date'])]
#[ApiFilter(EvenementIsActualite::class, properties: ['is_actuality'])]
#[ApiFilter(DateEvenementFilter::class, properties: ['date_month'])]
#[ApiFilter(DateEvenementYearFilter::class, properties: ['date_year'])]
#[ApiFilter(DatePreviousEvenementFilter::class, properties: ['date_previous'])]
class Evenement
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups('evenement:read')]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups('evenement:read')]
    private ?string $nom = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups('evenement:read')]
    private ?\DateTimeInterface $date = null;

    #[ORM\Column(length: 255)]
    #[Groups('evenement:read')]
    private ?string $lieu = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups('evenement:read')]
    private ?string $commentaire = null;

    #[ORM\Column(type: Types::BOOLEAN)]
    #[Groups('evenement:read')]
    private ?bool $is_actuality = null;

    #[ORM\ManyToOne(targetEntity: Organisateur::class, inversedBy: 'evenements')]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups('evenement:read')]
    private ?Organisateur $organisateur = null;

    #[ORM\OneToMany(targetEntity: Media::class, mappedBy: 'evenement', cascade: ['remove'])]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['evenement:read', 'media:read'])]
    private Collection $media;

    public function __construct()
    {
        $this->media = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;
        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): static
    {
        $this->date = $date;
        return $this;
    }

    public function getLieu(): ?string
    {
        return $this->lieu;
    }

    public function setLieu(string $lieu): static
    {
        $this->lieu = $lieu;
        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(string $commentaire): static
    {
        $this->commentaire = $commentaire;
        return $this;
    }

    public function getMedia(): Collection
    {
        return $this->media;
    }

    public function addMedia(Media $media): static
    {
        if (!$this->media->contains($media)) {
            $this->media->add($media);
            $media->setEvenement($this);
        }
        return $this;
    }

    public function removeMedia(Media $media): static
    {
        if ($this->media->removeElement($media) && $media->getEvenement() === $this) {
            $media->setEvenement(null);
        }
        return $this;
    }

    public function getOrganisateur(): ?Organisateur
    {
        return $this->organisateur;
    }

    public function setOrganisateur(?Organisateur $organisateur): static
    {
        $this->organisateur = $organisateur;
        return $this;
    }

    public function getIsActuality(): ?bool
    {
        return $this->is_actuality;
    }

    public function setIsActuality(?bool $is_actuality): void
    {
        $this->is_actuality = $is_actuality;
    }
}
