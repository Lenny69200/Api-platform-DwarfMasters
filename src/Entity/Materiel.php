<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Put;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Delete;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\MaterielRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

use ApiPlatform\Metadata\Post;

use App\Controller\Materiel\Post\MaterielController;
use App\Controller\Materiel\Update\MaterielUpdateController;

#[ORM\Entity(repositoryClass: MaterielRepository::class)]
#[ApiResource(operations: [
    new Get(),
    new GetCollection(),
    new Post(
        controller: MaterielController::class,
        deserialize: false,
    ),
    new Post(
        uriTemplate: '/materiels/{id}',
        requirements: ['id' => '\d+'],
        controller: MaterielUpdateController::class,
        deserialize: false,
    ),
    new Patch(),
    new Put(
        controller: MaterielUpdateController::class,
        deserialize: false,
    ),
    new Delete(),
])]
#[Vich\Uploadable]

class Materiel
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups('reservation:read')]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups('reservation:read')]
    private ?string $libelle = null;

    #[ORM\Column]
    #[Groups('reservation:read')]
    private ?int $prix = null;

    #[ORM\Column]
    #[Groups('reservation:read')]
    private ?bool $pourAssociation = null;

    #[ORM\Column]
    #[Groups('reservation:read')]
    private ?int $nbExemplaires = null;

    #[ORM\Column]
    #[Groups('reservation:read')]
    private ?int $nbExemplairesDispo = null;

    #[ORM\Column(nullable: true)]
    #[Groups('reservation:read')]
    private ?string $image = null;

    #[ORM\OneToMany(targetEntity: ReservationMateriel::class, mappedBy: 'materiel', cascade: ['remove'])]
    private Collection $reservationMateriels;

    /**
     * @Vich\UploadableField(mapping="materiels", fileNameProperty="image")
     */
    #[Vich\UploadableField(mapping: 'materiels', fileNameProperty: 'image')]
    private ?File $imageFile = null;

    public function __construct()
    {
        $this->reservationMateriels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): static
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): static
    {
        $this->prix = $prix;

        return $this;
    }

    public function isPourAssociation(): ?bool
    {
        return $this->pourAssociation;
    }

    public function setPourAssociation(bool $pourAssociation): static
    {
        $this->pourAssociation = $pourAssociation;

        return $this;
    }

    public function getNbExemplaires(): ?int
    {
        return $this->nbExemplaires;
    }

    public function setNbExemplaires(int $nbExemplaires): static
    {
        $this->nbExemplaires = $nbExemplaires;

        return $this;
    }

    public function getNbExemplairesDispo(): ?int
    {
        return $this->nbExemplairesDispo;
    }

    public function setNbExemplairesDispo(int $nbExemplairesDispo): static
    {
        $this->nbExemplairesDispo = $nbExemplairesDispo;

        return $this;
    }

    /**
     * @return Collection<int, ReservationMateriel>
     */
    public function getReservationMateriels(): Collection
    {
        return $this->reservationMateriels;
    }

    public function addReservationMateriel(ReservationMateriel $reservationMateriel): static
    {
        if (!$this->reservationMateriels->contains($reservationMateriel)) {
            $this->reservationMateriels->add($reservationMateriel);
            $reservationMateriel->setMateriel($this);
        }

        return $this;
    }

    public function removeReservationMateriel(ReservationMateriel $reservationMateriel): static
    {
        if ($this->reservationMateriels->removeElement($reservationMateriel) && $reservationMateriel->getMateriel() === $this) {
            $reservationMateriel->setMateriel(null);
        }

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): void
    {
        $this->image = $image;
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageFile(?File $imageFile): void
    {
        $this->imageFile = $imageFile;
    }

    #[Groups(['materiel:read'])]
    public function getImageUrl(): ?string
    {
        return $this->image ? 'uploads/materiels/' . $this->image : null;
    }
}
