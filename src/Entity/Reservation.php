<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Repository\ReservationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ReservationRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['reservation:read']],
)]class Reservation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups('reservation:read')]
    private ?int $id = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups('reservation:read')]
    private ?\DateTimeInterface $dateReservation = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups('reservation:read')]
    private ?\DateTimeInterface $dateRetour = null;

    #[ORM\OneToMany(targetEntity: ReservationMateriel::class, mappedBy: 'reservation', cascade: ['remove'])]
    #[Groups('reservation:read')]
    private Collection $reservationMateriels;

    #[ORM\ManyToOne(targetEntity: StatutReservation::class, inversedBy: 'reservation')]
    #[Groups('reservation:read')]
    private ?StatutReservation $statutReservation = null;

    #[ORM\ManyToOne(targetEntity: Association::class, inversedBy: 'reservation')]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups('reservation:read')]
    private ?Association $association = null;

    #[ORM\ManyToOne(targetEntity: Particulier::class, inversedBy: 'reservation')]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups('reservation:read')]
    private ?Particulier $particulier = null;

    public function __construct()
    {
        $this->reservationMateriels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateReservation(): ?\DateTimeInterface
    {
        return $this->dateReservation;
    }

    public function setDateReservation(\DateTimeInterface $dateReservation): static
    {
        $this->dateReservation = $dateReservation;

        return $this;
    }

    public function getDateRetour(): ?\DateTimeInterface
    {
        return $this->dateRetour;
    }

    public function setDateRetour(\DateTimeInterface $dateRetour): static
    {
        $this->dateRetour = $dateRetour;

        return $this;
    }

    /**
     * @return Collection<int, ReservationMateriel>
     */
    public function getReservationMateriels(): Collection
    {
        return $this->reservationMateriels;
    }

    public function addReservationMateriel(ReservationMateriel $reservationMateriel): static
    {
        if (!$this->reservationMateriels->contains($reservationMateriel)) {
            $this->reservationMateriels->add($reservationMateriel);
            $reservationMateriel->setReservation($this);
        }

        return $this;
    }

    public function removeReservationMateriel(ReservationMateriel $reservationMateriel): static
    {
        if ($this->reservationMateriels->removeElement($reservationMateriel) && $reservationMateriel->getReservation() === $this) {
            $reservationMateriel->setReservation(null);
        }

        return $this;
    }

    public function getStatutReservation(): ?StatutReservation
    {
        return $this->statutReservation;
    }

    public function setStatutReservation(?StatutReservation $statutReservation): void
    {
        $this->statutReservation = $statutReservation;
    }

    public function getAssociation(): ?Association
    {
        return $this->association;
    }

    public function setAssociation(?Association $association): void
    {
        $this->association = $association;
    }

    public function getParticulier(): ?Particulier
    {
        return $this->particulier;
    }

    public function setParticulier(?Particulier $particulier): void
    {
        $this->particulier = $particulier;
    }
}
