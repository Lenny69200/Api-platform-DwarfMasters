<?php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use App\Entity\Contact;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

final class MailProcessor implements ProcessorInterface
{
    public function __construct(
        #[Autowire(service: 'api_platform.doctrine.orm.state.persist_processor')]
        private ProcessorInterface $persistProcessor,
        private MailerInterface $mailer,
    )
    {
    }

    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = []): mixed
    {

        $result = $this->persistProcessor->process($data, $operation, $uriVariables, $context);
        $this->sendWelcomeEmail($data);

        return $result;
    }

    private function sendWelcomeEmail(Contact $contact): void
    {
        $this->mailer->send(
            (new Email())
                ->sender(
                    new Address(
                        $contact->getEmail(),
                        $contact->getName()
                    )
                )
                ->to(
                    new Address(
                        'l.bondoux@lyon.ort.asso.fr',
                        'Admin'
                    )
                )
                ->subject('Message de contact')
                ->text(
                    "Bonjour, vous avez reçu un message de {$contact->getName()} ({$contact->getEmail()}): 
                        {$contact->getMessage()}")

        );
    }
}
