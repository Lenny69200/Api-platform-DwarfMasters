<?php

namespace App\Filter;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;
use ApiPlatform\Metadata\Operation;
use DateTime;

class DatePreviousEvenementFilter extends AbstractFilter
{
    protected function filterProperty(
        string $property,
               $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        ?Operation $operation = null,
        array $context = []
    ): void
    {
        if ($property !== 'date_previous') {
            return;
        }

        $parameterName = $queryNameGenerator->generateParameterName('date_previous');
        $rootAlias = $queryBuilder->getRootAliases()[0];;

        $queryBuilder
            ->andWhere($queryBuilder->expr()->lt($rootAlias . '.date', ':' . $parameterName))
            ->setParameter($parameterName, new DateTime())
            ->orderBy($rootAlias . '.date', 'DESC');
    }

    public function getDescription(string $resourceClass): array
    {
        return [
            'date_previous' => [
                'property' => 'date_previous',
                'type' => 'string',
                'required' => false,
                'description' => 'Filter events that have a date before the specified date',
            ],
        ];
    }
}
