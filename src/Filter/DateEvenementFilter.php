<?php

namespace App\Filter;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use Doctrine\ORM\QueryBuilder;

class DateEvenementFilter extends AbstractFilter
{

    protected function filterProperty(
        string $property,
               $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        ?Operation $operation = null,
        array $context = []
    ): void
    {
        if ($property !== 'date_month') {
            return;
        }

        $rootAlias = $queryBuilder->getRootAliases()[0];
        $value = new \DateTime($value);
        $dateDebut = (clone $value)->modify('first day of this month')->format('Y-m-d');
        $dateEnd = (clone $value)->modify('last day of this month')->format('Y-m-d');

        $queryBuilder
            ->andWhere($queryBuilder->expr()->between($rootAlias . '.date', ':start', ':end'))
            ->setParameter('start', $dateDebut)
            ->setParameter('end', $dateEnd);
    }

    public function getDescription(string $resourceClass): array
    {
        return [
            'date_month' => [
                'property' => 'date_month',
                'type' => 'string',
                'required' => false,
                'swagger' => [
                    'type' => 'array'
                ],
            ],
        ];
    }
}
