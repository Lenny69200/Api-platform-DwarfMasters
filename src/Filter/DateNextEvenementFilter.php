<?php
namespace App\Filter;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;
use ApiPlatform\Metadata\Operation;

class DateNextEvenementFilter extends AbstractFilter
{
    protected function filterProperty(
        string $property,
               $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        ?Operation $operation = null,
        array $context = []
    ): void
    {
        if ($property !== 'date') {
            return;
        }

        $parameterName = $queryNameGenerator->generateParameterName('date');
        $rootAlias = $queryBuilder->getRootAliases()[0];

        $queryBuilder
            ->andWhere($queryBuilder->expr()->gte($rootAlias . '.date', ':' . $parameterName))
            ->setParameter($parameterName, new \DateTime())
            ->orderBy($rootAlias . '.date', 'ASC')
        ;
    }

    public function getDescription(string $resourceClass): array
    {
        return [
            'date' => [
                'property' => 'date',
                'type' => 'string',
                'required' => false,
                'description' => 'Filter events after today\'s date',
            ],
        ];
    }
}
