<?php

namespace App\Filter;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;
use ApiPlatform\Metadata\Operation;
use Symfony\Component\PropertyInfo\Type;

class EvenementIsActualite extends AbstractFilter
{
    protected function filterProperty(
        string $property,
               $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        ?Operation $operation = null,
        array $context = []
    ): void
    {
        if ($property !== 'is_actuality') {
            return;
        }

        $parameterName = $queryNameGenerator->generateParameterName($property);
        $rootAlias = $queryBuilder->getRootAliases()[0];

        // get all events when is_actualite is true
        $queryBuilder
            ->andWhere($queryBuilder->expr()->eq($rootAlias . '.is_actuality', ':' . $parameterName))
            ->setParameter($parameterName, $value);
    }

    public function getDescription(string $resourceClass): array
    {
        return [
            'is_actuality' => [
                'property' => 'is_actuality',
                'type' => Type::BUILTIN_TYPE_BOOL,
                'required' => false,
                'swagger' => [
                    'type' => 'boolean',
                ],
            ],
        ];
    }
}