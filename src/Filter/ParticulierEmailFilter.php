<?php

namespace App\Filter;

use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;
use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\QueryBuilder;

class ParticulierEmailFilter extends AbstractFilter
{

    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, ?\ApiPlatform\Metadata\Operation $operation = null, array $context = []): void
    {
        if ($property !== 'email') {
            return;
        }

        $alias = $queryBuilder->getRootAliases()[0];
        $queryBuilder
            ->andWhere($alias . '.email = :email')
            ->setParameter(':email', $value);
    }

    public function getDescription(string $resourceClass): array
    {
        return [
            'email' => [
                'property' => 'email',
                'type' => 'string',
                'required' => false,
                'description' => 'Filter particulier by email',
            ],
        ];
    }
}