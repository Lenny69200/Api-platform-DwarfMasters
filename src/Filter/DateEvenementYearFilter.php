<?php

namespace App\Filter;

use ApiPlatform\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use ApiPlatform\Metadata\Operation;
use Doctrine\ORM\QueryBuilder;
use ApiPlatform\Doctrine\Orm\Filter\AbstractFilter;

class DateEvenementYearFilter extends AbstractFilter
{
    protected function filterProperty(
        string $property,
               $value,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        ?Operation $operation = null,
        array $context = []
    ): void
    {
        if ($property !== 'date_year') {
            return;
        }

        $rootAlias = $queryBuilder->getRootAliases()[0];
        $value = new \DateTime($value);
        $dateDebut = $value->format('Y-01-01');
        $dateEnd = $value->format('Y-12-31');

        $queryBuilder
            ->andWhere($queryBuilder->expr()->between($rootAlias . '.date', ':start', ':end'))
            ->setParameter('start', $dateDebut)
            ->setParameter('end', $dateEnd);
    }

    public function getDescription(string $resourceClass): array
    {
        return [
            'date_year' => [
                'property' => 'date_year',
                'type' => 'string',
                'required' => false,
                'swagger' => [
                    'type' => 'array'
                ],
            ],
        ];
    }
}