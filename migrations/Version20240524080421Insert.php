<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240524080421Insert extends AbstractMigration
{
    public function getDescription(): string
    {
        return '➕ Insertion des données';
    }

    public function up(Schema $schema): void
    {
        // Table Statut_Reservations
        $this->addSql("INSERT INTO statut_reservation (id, libelle) VALUES
            (1, 'Demandée'),
            (2, 'En cours'),
            (3, 'Terminée'),
            (4, 'Annulée')");

        // Table Roles
        $this->addSql("INSERT INTO role (id, libelle) VALUES
            (1, 'Administrateur'),
            (2, 'Association')");

        // Utilisateur fixture
        $hashedPassword = password_hash('admin', PASSWORD_BCRYPT);
        $hashedPassword2 = password_hash('asso', PASSWORD_BCRYPT);
        $this->addSql("INSERT INTO utilisateur (id, login, password, role_id) VALUES
            (1, 'admin@gmail.com', '$hashedPassword', 1)");
        $this->addSql("INSERT INTO utilisateur (id, login, password, role_id) VALUES
            (2, 'asso@gmail.com', '$hashedPassword2', 2)");

        // Table Materiels
        $this->addSql("INSERT INTO materiel (id, libelle, prix, pour_association, nb_exemplaires, nb_exemplaires_dispo, image) VALUES
            (1, 'Parapluie 3x4 m', 15, false, 4, 4, 'parapluie.jpg'),
            (2, 'Table pliante 2 m', 20, false, 9, 9, 'table_pliante.jpeg'),
            (3, 'Table pliante 2,2 m', 20, false, 35, 35, 'table_pliante.jpeg'),
            (4, 'Banc 2 m', 20, false, 41, 41, 'banc.jpg'),
            (5, 'Banc pliant 2,2 m', 20, false, 78, 78, 'banc.jpg'),
            (6, 'Tente pliante 4.5x3 m', 35, false, 1, 1, 'tente.jpg'),
            (7, 'Tente pliante 3x3 m', 25, false, 3, 3, 'tente.jpg'),
            (8, 'Tente pliante 6x3 m', 50, false, 4, 4, 'tente.jpg'),
            (9, 'Chauffe saucisses', 20, false, 2, 2, 'saucisse.jpg'),
            (10, 'Crêpière', 20, false, 2, 2, 'crepiere.jpg'),
            (11, 'Congélateur bahut 200 litres', 30, false, 1, 1, 'congelateur.jpeg'),
            (12, 'Mange-Debout', 5, false, 6, 6, 'mangedebout.jpg'),
            (13, 'Remorque frigorifique 6m3', 100, false, 1, 1, 'remorque.jpg'),
            (14, 'Remorque réfrigérée', 50, true, 5, 3, 'remorque.jpg') ");

        // Table Associations
        $this->addSql("INSERT INTO association (id, nom, telephone, email, utilisateur_id) VALUES
            (-1, 'Association sportive', '0123456789', 'associationsportive@exemple.com', 2),
            (-2, 'Association culturelle', '0987654321', 'associationculturelle@exemple.com', NULL),
            (-3, 'Association caritative', '0456789012', 'associationcaritative@exemple.com', NULL)");

        // Table Organisateurs
        $this->addSql("INSERT INTO organisateur (id, nom) VALUES
            (1, 'Association Remini'),
            (2, 'Société Orientale'),
            (3, 'Collectivité DEF')");

        // Table Particuliers
        $this->addSql("INSERT INTO particulier (id, nom, prenom, telephone, email) VALUES
            (-1, 'Dupont', 'Jean', '0123456789', 'jeandupont@exemple.com'),
            (-2, 'Martin', 'Marie', '0987654321', 'mariemartin@exemple.com'),
            (-3, 'Durant', 'Pierre', '0456789012', 'pierredurant@exemple.com')");

        // Table Evenements
        $events = [
            // Adding events for each month of 2024
            ['id' => 1, 'nom' => 'Nouvel An', 'date' => '2024-01-01', 'lieu' => 'Paris', 'commentaire' => '', 'is_actuality' => false, 'organisateur_id' => 1],
            ['id' => 2, 'nom' => 'Festival de la Galette', 'date' => '2024-01-10', 'lieu' => 'Lyon', 'commentaire' => '', 'is_actuality' => false, 'organisateur_id' => 2],
            ['id' => 3, 'nom' => 'Carnaval', 'date' => '2024-02-20', 'lieu' => 'Nice', 'commentaire' => '', 'is_actuality' => true, 'organisateur_id' => 3],
            ['id' => 4, 'nom' => 'Fête de la Saint-Patrick', 'date' => '2024-03-17', 'lieu' => 'Bordeaux', 'commentaire' => '', 'is_actuality' => false, 'organisateur_id' => 1],
            ['id' => 5, 'nom' => 'Fête des Fleurs', 'date' => '2024-04-25', 'lieu' => 'Toulouse', 'commentaire' => '', 'is_actuality' => true, 'organisateur_id' => 2],
            ['id' => 6, 'nom' => 'Marathon de Paris', 'date' => '2024-05-15', 'lieu' => 'Paris', 'commentaire' => '', 'is_actuality' => false, 'organisateur_id' => 3],
            ['id' => 7, 'nom' => 'Fête de la Musique', 'date' => '2024-06-21', 'lieu' => 'Marseille', 'commentaire' => '', 'is_actuality' => true, 'organisateur_id' => 1],
            ['id' => 8, 'nom' => 'Tournoi de Tennis', 'date' => '2024-07-05', 'lieu' => 'Lyon', 'commentaire' => '', 'is_actuality' => false, 'organisateur_id' => 2],
            ['id' => 9, 'nom' => 'Feu d\'artifice du 14 juillet', 'date' => '2024-07-14', 'lieu' => 'Paris', 'commentaire' => '', 'is_actuality' => true, 'organisateur_id' => 3],
            ['id' => 10, 'nom' => 'Festival du Vin', 'date' => '2024-08-18', 'lieu' => 'Bordeaux', 'commentaire' => '', 'is_actuality' => false, 'organisateur_id' => 1],
            ['id' => 11, 'nom' => 'Fête de la rentrée', 'date' => '2024-09-01', 'lieu' => 'Lyon', 'commentaire' => '', 'is_actuality' => true, 'organisateur_id' => 2],
            ['id' => 12, 'nom' => 'Fête des Vendanges', 'date' => '2024-10-15', 'lieu' => 'Dijon', 'commentaire' => '', 'is_actuality' => false, 'organisateur_id' => 3],
            ['id' => 13, 'nom' => 'Halloween', 'date' => '2024-10-31', 'lieu' => 'Strasbourg', 'commentaire' => '', 'is_actuality' => true, 'organisateur_id' => 1],
            ['id' => 14, 'nom' => 'Marché de Noël', 'date' => '2024-12-05', 'lieu' => 'Colmar', 'commentaire' => '', 'is_actuality' => false, 'organisateur_id' => 2],
            ['id' => 15, 'nom' => 'Réveillon de Noël', 'date' => '2024-12-24', 'lieu' => 'Paris', 'commentaire' => '', 'is_actuality' => true, 'organisateur_id' => 3],

            // Adding events for each year from 2020 to 2023
            ['id' => 16, 'nom' => 'Concert de l\'An 2020', 'date' => '2020-01-20', 'lieu' => 'Lyon', 'commentaire' => '', 'is_actuality' => true, 'organisateur_id' => 1],
            ['id' => 17, 'nom' => 'Festival de l\'été 2021', 'date' => '2021-06-21', 'lieu' => 'Marseille', 'commentaire' => '', 'is_actuality' => true, 'organisateur_id' => 2],
            ['id' => 18, 'nom' => 'Rallye de la Saint-Jean 2022', 'date' => '2022-06-24', 'lieu' => 'Bordeaux', 'commentaire' => '', 'is_actuality' => true, 'organisateur_id' => 3],
            ['id' => 19, 'nom' => 'Bal des Pompiers 2023', 'date' => '2023-07-14', 'lieu' => 'Paris', 'commentaire' => '', 'is_actuality' => true, 'organisateur_id' => 1],
        ];

        foreach ($events as $event) {
            $eventName = str_replace("'", "''", $event['nom']);
            $this->addSql("INSERT INTO evenement (id, nom, date, lieu, commentaire, is_actuality, organisateur_id) VALUES
                ({$event['id']}, '{$eventName}', '{$event['date']}', '{$event['lieu']}', '{$event['commentaire']}', " . ($event['is_actuality'] ? 'true' : 'false') . ", {$event['organisateur_id']})");
        }

        // Table Type_Medias
        $this->addSql("INSERT INTO type_media (id, libelle) VALUES
            (1, 'image')");

        // Table Media
        $this->addSql("INSERT INTO media (id, chemin, description, evenement_id, type_media_id) VALUES
            (1, 'fete1.png', 'Image correspondant à une famille', 1, 1),
            (2, 'fete2.png', 'Image correspondant à une fratrie', 2, 1),
            (3, 'fete3.jpg', 'Image correspondant à une équipe', 3, 1),
            (4, 'fete4.jpg', 'Image correspondant à une équipe', 4, 1),
            (5, 'fete5.jpg', 'Image correspondant à une équipe', 5, 1),
            (6, 'fete6.jpg', 'Image correspondant à une équipe', 6, 1),
            (7, 'fete7.png', 'Image correspondant à une équipe', 7, 1),
            (8, 'fete8.png', 'Image correspondant à une équipe', 8, 1),
            (9, 'fete9.png', 'Image correspondant à une équipe', 9, 1),
            (10, 'fete10.png', 'Image correspondant à une équipe', 10, 1),
            (11, 'fete11.png', 'Image correspondant à une équipe', 11, 1),
            (12, 'fete12.png', 'Image correspondant à une équipe', 12, 1),
            (13, 'fete13.png', 'Image correspondant à une équipe', 13, 1),
            (14, 'fete14.png', 'Image correspondant à une équipe', 14, 1),
            (15, 'fete15.png', 'Image correspondant à une équipe', 15, 1),
            (16, 'fete16.png', 'Image correspondant à une équipe', 16, 1),
            (17, 'fete17.png', 'Image correspondant à une équipe', 17, 1),
            (18, 'fete18.png', 'Image correspondant à une équipe', 18, 1),
            (19, 'fete19.png', 'Image correspondant à une équipe', 19, 1)");
    }

    public function down(Schema $schema): void
    {
        // Vous pouvez ajouter des requêtes pour supprimer les données insérées ici si nécessaire
        // pour rendre cette migration réversible.
    }
}
