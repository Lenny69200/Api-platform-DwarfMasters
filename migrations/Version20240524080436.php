<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240524080436 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '➕ Add sequence values to tables if needed';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('SELECT SETVAL(\'statut_reservation_id_seq\', (SELECT COALESCE(MAX(id), 1) FROM statut_reservation));');
        $this->addSql('SELECT SETVAL(\'role_id_seq\', (SELECT COALESCE(MAX(id), 1) FROM role));');
        $this->addSql('SELECT SETVAL(\'utilisateur_id_seq\', (SELECT COALESCE(MAX(id), 1) FROM utilisateur));');
        $this->addSql('SELECT SETVAL(\'materiel_id_seq\', (SELECT COALESCE(MAX(id), 1) FROM materiel));');
        $this->addSql('SELECT SETVAL(\'organisateur_id_seq\', (SELECT COALESCE(MAX(id), 1) FROM organisateur));');
        $this->addSql('SELECT SETVAL(\'evenement_id_seq\', (SELECT COALESCE(MAX(id), 1) FROM evenement));');
        $this->addSql('SELECT SETVAL(\'reservation_id_seq\', (SELECT COALESCE(MAX(id), 1) FROM reservation));');
        $this->addSql('SELECT SETVAL(\'reservation_materiel_id_seq\', (SELECT COALESCE(MAX(id), 1) FROM reservation_materiel));');
        $this->addSql('SELECT SETVAL(\'type_media_id_seq\', (SELECT COALESCE(MAX(id), 1) FROM type_media));');
        $this->addSql('SELECT SETVAL(\'media_id_seq\', (SELECT COALESCE(MAX(id), 1) FROM media));');
    }

    public function down(Schema $schema): void
    {
        //DO NOTHING
    }
}
