-- Table Statut_Reservations
INSERT INTO statut_reservation (id, libelle)
VALUES
(1, 'En cours'),
(2, 'Validée'),
(3, 'Annulée');

-- Table Roles
INSERT INTO role (id, libelle) VALUES
(1, 'Administrateur'),
(2, 'Association');

-- Table Utilisateurs
INSERT INTO utilisateur (id, login, password, role_id) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1), -- Password ADMIN123
(2, 'association', '5ab9843f8c44a148fb3b4a5b2e27a1c6', 2); -- Password ASSOCIATION123


-- Table Materiels
INSERT INTO materiel (id, libelle, prix,pour_association, nb_exemplaires, nb_exemplaires_dispo) VALUES
(1, 'Projecteur', 50,true, 5, 3),
(2, 'Ordinateur portable',3 ,false, 10, 5),
(3, 'Appareil photo', 20,false, 7, 7);

-- Table Associations
INSERT INTO association (id, nom, telephone, email) VALUES
(1,'Association sportive', '0123456789', 'association.sportive@exemple.com'),
(2,'Association culturelle', '0987654321', 'association.culturelle@exemple.com'),
(3,'Association caritative', '0456789012', 'association.caritative@exemple.com');

-- Table Organisateurs
INSERT INTO organisateur (id, nom) VALUES
(1, 'Association Remini'),
(2, 'Société Orientale'),
(3, 'Collectivité DEF');

-- Table Evenements
INSERT INTO evenement (id, nom, date, lieu, commentaire, isActuality, organisateur_id) VALUES
(1, 'Fête des classes au Stade', '2022-07-01', 'Lyon', '',false, 1),
(2, 'Fête de l Eté à la Halle organisée en partenariat avec la Mairie', '2022-05-01', 'Halle','',false, 2),
(3, 'Randonnée pédestre co-organisée par le comité des fêtes et la retraite sportive', '2022-05-10', 'Lyon','',true, 3);

-- Table Particuliers
INSERT INTO particulier (id, nom, prenom, telephone, email) VALUES
(1,'Dupont', 'Jean', '0123456789', 'jean.dupont@exemple.com'),
(2,'Martin', 'Marie', '0987654321', 'marie.martin@exemple.com'),
(3,'Durant', 'Pierre', '0456789012', 'pierre.durant@exemple.com');

-- Table Reservations
INSERT INTO reservation (id, date_reservation, date_retour, statut_reservation_id, particulier_id, association_id) VALUES
(1, '2024-05-22', '2024-05-25', 1, null, 1),
(2, '2024-05-23', '2024-05-26', 2, 1, null);

-- Table Reservation_Materiels
INSERT INTO reservation_materiel (id, reservation_id, materiel_id, quantite) VALUES
(1, 1, 1, 1),
(2, 2, 2, 1),
(4, 2, 3, 1);

-- Table Type_Medias
INSERT INTO type_media (id, libelle) VALUES
(1, 'image');

-- Table Media
INSERT INTO media (id, chemin, description, evenement_id, type_media_id) VALUES
(1, 'fete-des-classes1.jpg', '', 1, 1),
(2, 'fete-des-classes2.jpg', '', 1, 1),
(3, 'fete-des-classes3.jpg', '', 1, 1),
(4, 'fete-des-classes4.jpg', '', 1, 1),
(5, 'fete-des-classes5.jpg', '', 1, 1),
(6, 'fete-des-classes6.jpg', '', 1, 1),
(7, 'fete-des-classes7.jpg', '', 1, 1),
(8, 'fete-des-classes8.jpg', '', 1, 1),
(9, 'fete-des-classes9.jpg', '', 1, 1),
(10, 'fete-des-classes10.jpg', '', 1, 1),
(11, 'fete-des-classes11.jpg', '', 1, 1),
(12, 'Fete-été1.jpg', '', 2, 1),
(13, 'Fete-été2.jpg', '', 2, 1),
(14, 'Fete-été3.jpg', '', 2, 1),
(15, 'Fete-été4.jpg', '', 2, 1),
(16, 'Fete-été5.jpg', '', 2, 1),
(17, 'Fete-été6.jpg', '', 2, 1),
(18, 'Randonnée1.jpg', '', 3, 1),
(19, 'Randonnée2.jpg', '', 3, 1),
(20, 'Randonnée3.jpg', '', 3, 1),
(21, 'Randonnée4.jpg', '', 3, 1),
(22, 'Randonnée5.jpg', '', 3, 1),
(23, 'Randonnée6.jpg', '', 3, 1),
(24, 'Randonnée7.jpg', '', 3, 1);