-- Vérifiez la valeur actuelle de la séquence
SELECT last_value FROM statut_reservation_id_seq;

-- Trouvez la valeur maximale de l'ID dans la table
SELECT MAX(id) FROM statut_reservation;

-- Ajustez la séquence pour qu'elle continue à partir de la valeur maximale
SELECT SETVAL('statut_reservation_id_seq', (SELECT MAX(id) FROM statut_reservation));

-- Vérifiez la valeur actuelle de la séquence
SELECT last_value FROM role_id_seq;

-- Trouvez la valeur maximale de l'ID dans la table
SELECT MAX(id) FROM role;

-- Ajustez la séquence pour qu'elle continue à partir de la valeur maximale
SELECT SETVAL('role_id_seq', (SELECT MAX(id) FROM role));

-- Vérifiez la valeur actuelle de la séquence
SELECT last_value FROM utilisateur_id_seq;

-- Trouvez la valeur maximale de l'ID dans la table
SELECT MAX(id) FROM utilisateur;

-- Ajustez la séquence pour qu'elle continue à partir de la valeur maximale
SELECT SETVAL('utilisateur_id_seq', (SELECT MAX(id) FROM utilisateur));

-- Vérifiez la valeur actuelle de la séquence
SELECT last_value FROM materiel_id_seq;

-- Trouvez la valeur maximale de l'ID dans la table
SELECT MAX(id) FROM materiel;

-- Ajustez la séquence pour qu'elle continue à partir de la valeur maximale
SELECT SETVAL('materiel_id_seq', (SELECT MAX(id) FROM materiel));

-- Vérifiez la valeur actuelle de la séquence
SELECT last_value FROM organisateur_id_seq;

-- Trouvez la valeur maximale de l'ID dans la table
SELECT MAX(id) FROM organisateur;

-- Ajustez la séquence pour qu'elle continue à partir de la valeur maximale
SELECT SETVAL('organisateur_id_seq', (SELECT MAX(id) FROM organisateur));

-- Vérifiez la valeur actuelle de la séquence
SELECT last_value FROM evenement_id_seq;

-- Trouvez la valeur maximale de l'ID dans la table
SELECT MAX(id) FROM evenement;

-- Ajustez la séquence pour qu'elle continue à partir de la valeur maximale
SELECT SETVAL('evenement_id_seq', (SELECT MAX(id) FROM evenement));

-- Vérifiez la valeur actuelle de la séquence
SELECT last_value FROM reservation_id_seq;

-- Trouvez la valeur maximale de l'ID dans la table
SELECT MAX(id) FROM reservation;

-- Ajustez la séquence pour qu'elle continue à partir de la valeur maximale
SELECT SETVAL('reservation_id_seq', (SELECT MAX(id) FROM reservation));

-- Vérifiez la valeur actuelle de la séquence
SELECT last_value FROM reservation_materiel_id_seq;

-- Trouvez la valeur maximale de l'ID dans la table
SELECT MAX(id) FROM reservation_materiel;

-- Ajustez la séquence pour qu'elle continue à partir de la valeur maximale
SELECT SETVAL('reservation_materiel_id_seq', (SELECT MAX(id) FROM reservation_materiel));

-- Vérifiez la valeur actuelle de la séquence
SELECT last_value FROM type_media_id_seq;

-- Trouvez la valeur maximale de l'ID dans la table
SELECT MAX(id) FROM type_media;

-- Ajustez la séquence pour qu'elle continue à partir de la valeur maximale
SELECT SETVAL('type_media_id_seq', (SELECT MAX(id) FROM type_media));

-- Vérifiez la valeur actuelle de la séquence
SELECT last_value FROM media_id_seq;

-- Trouvez la valeur maximale de l'ID dans la table
SELECT MAX(id) FROM media;

-- Ajustez la séquence pour qu'elle continue à partir de la valeur maximale
SELECT SETVAL('media_id_seq', (SELECT MAX(id) FROM media));
